﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject[] balls;
    public Vector3 spawnValues;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;
    int randBall;
    void Start()
    {
        StartCoroutine(spawnBall());
    }

    
    void Update()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }
    IEnumerator spawnBall()
    {
        yield return new WaitForSeconds(startWait);
        while (stop)
        {
            randBall = Random.Range(0, 2);

            Vector3 spawnPosition = new Vector3(Random.Range(-20.0f, 20.0f), 1, Random.Range(-20.0f, 20.0f));

            Instantiate(balls[randBall], spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);

            yield return new WaitForSeconds(spawnWait);
        }
    }
}
