﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collission : MonoBehaviour
{
    //Volume:0..1
    //Magnitude

    public AudioSource _as;
    public AudioClip collisionSound;

    private void Start()
    {
        _as = GetComponent<AudioSource>();
        if (_as == null)
            Debug.Log("null");
        }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("hello");
        if (collision.gameObject.tag =="Collide"){
            _as.PlayOneShot(collisionSound);
        }
    }

}
