﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oscilator : MonoBehaviour
{
    public GameObject[] balls;
    public float spawnWait= 2;
    Random random;

    void Start()
    {
        random = new Random();
    }


    void Update()
    {
        spawnWait -= Time.deltaTime;
        if (spawnWait <= 0)
        {
            Instantiate(balls[random.Next(0,1)], new Vector3(Random.Range(-20.0f, 20.0f), transform.position.y, Random.Range(-20.0f, 20.0f)), transform.rotation);
            spawnWait = 2;

            
        }
    }


}
